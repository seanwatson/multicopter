/**************************************
 *
 * File:    multicopter.scad
 * Author:  Sean Waston
 * Date:    April 2014
 * License: MIT
 *
 **************************************/

include <dimensions.scad>

use <arm.scad>
use <body.scad>
use <leg.scad>
use <motor_mount.scad>

use <MCAD/triangles.scad>

/***** Modules *****/

module arm_with_leg() {
  arm();
  translate([ARM_LENGTH/2 - LEG_OFFSET, -ARM_WIDTH/2, -ARM_HEIGHT/2])
    rotate([-90, 0, 0])
      leg();
}

module arm_with_motor_mount(use_leg=false) {
  translate([MOTOR_MOUNT_WALL_THICKNESS - MOTOR_MOUNT_RADIUS - ARM_LENGTH,
             0, 0]) {
    translate([MOTOR_MOUNT_RADIUS - MOTOR_MOUNT_WALL_THICKNESS + ARM_LENGTH/2,
               0, (MOTOR_MOUNT_INTERFACE_WALL_THICKNESS + ARM_HEIGHT)/2])
      if(use_leg) {
        arm_with_leg();
      } else {
        arm();
      }
    motor_mount();
  }
}

/***** Renders *****/

for(i = [0:NUMBER_OF_ROTORS]) {
  rotate([0, 0, i*360/NUMBER_OF_ROTORS])
    translate([(-BODY_WIDTH*tan(180/NUMBER_OF_ROTORS)) +
                   (ARM_SLIT_LENGTH +
                      ARM_WIDTH/2/tan(90 - (180/NUMBER_OF_ROTORS)))*
                        cos(90 - (180/NUMBER_OF_ROTORS)),
               (-BODY_WIDTH) +
                   (ARM_SLIT_LENGTH +
                      ARM_WIDTH/2/tan(90 - (180/NUMBER_OF_ROTORS)))*
                        sin(90 - (180/NUMBER_OF_ROTORS)),
               0])
      rotate([0, 0, 90 - (180/NUMBER_OF_ROTORS)])
        arm_with_motor_mount(use_leg=(i % ROTORS_PER_LEG == 0));
}
translate([0, 0, (-ARM_SLIT_HEIGHT + ARM_WIDTH +
                   MOTOR_MOUNT_INTERFACE_WALL_THICKNESS)/2])
  body();
