/**************************************
 *
 * File:    body.scad
 * Author:  Sean Waston
 * Date:    April 2014
 * License: MIT
 *
 **************************************/

include <dimensions.scad>

// TODO: once own holes are made this will no longer depend
use <motor_mount.scad>

use <MCAD/triangles.scad>

/***** Modules *****/

// TODO: make these their own thing
module body_interface_holes() {
  motor_mount_interface_holes();
}

module body_triangle() {
  difference() {
    triangle(BODY_WIDTH, BODY_WIDTH*tan(180/NUMBER_OF_ROTORS),
             ARM_SLIT_HEIGHT);
    translate([BODY_WALL_THICKNESS/4,
               BODY_WALL_THICKNESS/4,
               0])
      triangle(BODY_WIDTH - BODY_WALL_THICKNESS,
                BODY_WIDTH*tan(180/NUMBER_OF_ROTORS) - BODY_WALL_THICKNESS,
                ARM_SLIT_HEIGHT);
    translate([BODY_WIDTH*tan(180/NUMBER_OF_ROTORS)-
               (ARM_WIDTH/2/sin(90 - 180/NUMBER_OF_ROTORS)), 0, 0])
        rotate([0, 0, -180 + 180/NUMBER_OF_ROTORS])
          translate([(-ARM_WIDTH-0.5)/2, 0, 0])
            triangle((ARM_WIDTH + 1)/2/tan(90 - (180/NUMBER_OF_ROTORS)),
                     (ARM_WIDTH+1)/2,
                     ARM_SLIT_HEIGHT);
  }
}

module body_piece() {
  translate([0, -BODY_WIDTH, 0]) {
    body_triangle();
    mirror()
      body_triangle();
  }
}

module body() {
  for(i = [0:360/NUMBER_OF_ROTORS:360]) {
    rotate([0, 0, i])
      body_piece();
  }
}

/***** Renders *****/

body();
