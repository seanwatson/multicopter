/**************************************
 *
 * File: motor_mount.scad
 * Author: Sean Waston
 * Date: April 2014
 * License: MIT
 *
 **************************************/

include <dimensions.scad>

use <MCAD/shapes.scad>
use <MCAD/triangles.scad>

/***** Modules *****/

module motor_mount() {
  difference() {
    union() {
      translate([MOTOR_MOUNT_RADIUS - MOTOR_MOUNT_WALL_THICKNESS +
                  MOTOR_MOUNT_INTERFACE_LENGTH/2, 0, 0])
        slot_motor_mount_interface();
      motor_mount_body();
    }
    translate([0, 0, CROSS_BAR_HEIGHT/2])
    cylinder(h=CROSS_BAR_HEIGHT, r=MOTOR_MOUNT_CENTER_HOLE_RADIUS,
             center=true);
  }
}

module motor_mount_body() {
  tube(MOTOR_MOUNT_HEIGHT, MOTOR_MOUNT_RADIUS, MOTOR_MOUNT_WALL_THICKNESS);
  cross_bar(CROSS_BAR_HOLE_OFFSET_RADIUS_1);
  rotate([0, 0, CROSS_BAR_ANGLE])
    cross_bar(CROSS_BAR_HOLE_OFFSET_RADIUS_2);
}

module cross_bar(hole_offset_radius) {
  translate([0, 0, CROSS_BAR_HEIGHT/2])
    difference() {
      cube([(MOTOR_MOUNT_RADIUS - MOTOR_MOUNT_WALL_THICKNESS)*2,
            CROSS_BAR_WIDTH, CROSS_BAR_HEIGHT], center=true);
      for(i = [0:180:360])
        rotate([0, 0, i])
          translate([hole_offset_radius,0,0])
            cylinder(h=CROSS_BAR_HEIGHT, r=CROSS_BAR_HOLE_RADIUS, center=true);
    }
}

module slot_motor_mount_interface() {
  translate([0, (MOTOR_MOUNT_INTERFACE_WALL_THICKNESS + ARM_WIDTH)/2,
             ARM_HEIGHT/2])
    half_slot_motor_mount_interface();
  translate([0, -(MOTOR_MOUNT_INTERFACE_WALL_THICKNESS + ARM_WIDTH)/2,
             ARM_HEIGHT/2])
    half_slot_motor_mount_interface();
  translate([-MOTOR_MOUNT_INTERFACE_LENGTH/2, 0, ARM_HEIGHT-0.1])
    rotate([90, -90, 180])
      motor_mount_interface_support_pair();
}

module half_slot_motor_mount_interface() {
  rotate([90, 0, 0])
    difference() {
      cube([MOTOR_MOUNT_INTERFACE_LENGTH,
            ARM_HEIGHT,
            MOTOR_MOUNT_INTERFACE_WALL_THICKNESS],
            center=true);
      translate([-MOTOR_MOUNT_INTERFACE_HOLE_OFFSET, 0, 0])
        motor_mount_interface_holes();
    }
}

module rectangular_motor_mount_interface() {
  translate([0, 0, (ARM_HEIGHT + MOTOR_MOUNT_INTERFACE_WALL_THICKNESS)/2])
    difference() {
      cube([MOTOR_MOUNT_INTERFACE_LENGTH,
            ARM_WIDTH + MOTOR_MOUNT_INTERFACE_WALL_THICKNESS,
            ARM_HEIGHT + MOTOR_MOUNT_INTERFACE_WALL_THICKNESS],
           center=true);
      cube([MOTOR_MOUNT_INTERFACE_LENGTH, ARM_WIDTH, ARM_HEIGHT], center=true);
      translate([-MOTOR_MOUNT_INTERFACE_HOLE_OFFSET, 0, 0])
        motor_mount_interface_holes();
    }
  for(i = [0:2:2])
    translate([-MOTOR_MOUNT_INTERFACE_LENGTH/2,
               (-(i - 1) % 2)*
                 (ARM_WIDTH + MOTOR_MOUNT_INTERFACE_WALL_THICKNESS)/2,
               (i % 2)*(ARM_HEIGHT + MOTOR_MOUNT_INTERFACE_WALL_THICKNESS) +
                 (((i + 1) % 2)*
                 (ARM_HEIGHT + MOTOR_MOUNT_INTERFACE_WALL_THICKNESS)/2)])
    rotate([(i - 1)*90, 0, 0])
      rotate([0, -90, -90])
        motor_mount_interface_support_pair();
}

module motor_mount_interface_holes() {
  for(i = [0:1])
    mirror([i, 0, 0])
      translate([MOTOR_MOUNT_INTERFACE_HOLE_SPACING/2, 0,
                 -(ARM_HEIGHT + MOTOR_MOUNT_INTERFACE_WALL_THICKNESS)/2])
        motor_mount_interface_hole();
}


module motor_mount_interface_hole() {
    cylinder(h=ARM_HEIGHT + MOTOR_MOUNT_INTERFACE_WALL_THICKNESS,
             r=MOTOR_MOUNT_INTERFACE_HOLE_RADIUS);
}


module motor_mount_interface_support_pair() {
  for(i = [0:1])
    mirror([0, 0, i])
      translate([0, 0,
                 ARM_WIDTH/2])
        motor_mount_interface_support();
}

module motor_mount_interface_support() {
  triangle(MOTOR_MOUNT_INTERFACE_SUPPORT_LENGTH,
           MOTOR_MOUNT_INTERFACE_SUPPORT_HEIGHT,
           MOTOR_MOUNT_INTERFACE_SUPPORT_WALL_THICKNESS);
}

/***** Renders *****/

$fn = 100;
motor_mount();
