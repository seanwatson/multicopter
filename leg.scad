/**************************************
 *
 * File:    leg.scad
 * Author:  Sean Waston
 * Date:    April 2014
 * License: MIT
 *
 **************************************/

include <dimensions.scad>

use <MCAD/triangles.scad>

/***** Modules *****/

module half_leg() {
  difference() {
    triangle(LEG_HEIGHT, LEG_WIDTH/2, ARM_WIDTH);
    translate([0, LEG_WALL_THICKNESS, 0])
      triangle((LEG_WIDTH/2 - LEG_WALL_THICKNESS)*LEG_HEIGHT/LEG_WIDTH*2,
               LEG_WIDTH/2 - LEG_WALL_THICKNESS,
               ARM_WIDTH);
  }
  cube([LEG_WALL_THICKNESS/2, LEG_HEIGHT-2*LEG_WALL_THICKNESS-1, ARM_WIDTH]);
}

module leg_holes() {
  for (i = [0:1])
    mirror([i, 0, 0])
      rotate([90, 0, 0])
        translate([LEG_HOLE_OFFSET, ARM_WIDTH/2, -LEG_WALL_THICKNESS])
          cylinder(h=LEG_WALL_THICKNESS + ARM_HEIGHT,
                   r=LEG_HOLE_RADIUS);
}

module leg() {
  difference() {
    union () {
      half_leg();
      mirror()
        half_leg();
    }
    leg_holes();  
  }
}

/***** Renders *****/

$fn = 100;
leg();
