/**************************************
 *
 * File:    arm.scad
 * Author:  Sean Waston
 * Date:    April 2014
 * License: MIT
 *
 **************************************/

include <dimensions.scad>

use <body.scad>
use <leg.scad>
use <motor_mount.scad>

/***** Modules *****/

module arm() {
  difference() {
    union() {
      cube([ARM_LENGTH, ARM_WIDTH, ARM_HEIGHT], center=true);
      if (USE_HOLLOW_ARMS) {
        cube([ARM_LENGTH, ARM_WIDTH - ARM_WALL_THICKNESS,
              ARM_HEIGHT-ARM_WALL_THICKNESS], center=true);
      }
      translate([(ARM_LENGTH - ARM_SLIT_LENGTH)/2, 0, 0])
        cube([ARM_SLIT_LENGTH, ARM_WIDTH, ARM_HEIGHT], center=true);
    }
    translate([(-ARM_LENGTH + MOTOR_MOUNT_INTERFACE_LENGTH)/2, 0, 0])
      rotate([90, 0, 0])
       motor_mount_interface_holes();
    translate([(ARM_LENGTH-ARM_SLIT_LENGTH)/2,0,0])
      cube([ARM_SLIT_LENGTH, ARM_WIDTH, ARM_SLIT_HEIGHT], center=true);
    translate([(ARM_LENGTH - BODY_INTERFACE_OFFSET)/2, 0, 0])
      body_interface_holes();
    translate([-LEG_OFFSET+ARM_LENGTH/2, -ARM_WIDTH/2, -ARM_HEIGHT/2])
      rotate([-90, 0 , 0])
       leg_holes();
  }
}

/***** Renders *****/

$fn = 100;
arm();
